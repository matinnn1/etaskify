package az.ingress.etaskify.mapper;

import az.ingress.etaskify.dto.Request.OrganizationRequestDto;
import az.ingress.etaskify.dto.Response.OrganizationResponseDto;
import az.ingress.etaskify.entity.Organization;
import az.ingress.etaskify.entity.Organization.OrganizationBuilder;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-02-21T12:08:19+0400",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 17.0.10 (Oracle Corporation)"
)
@Component
public class OrganizationMapperImpl implements OrganizationMapper {

    @Override
    public Organization requestDtoToEntity(OrganizationRequestDto organizationRequestDto) {
        if ( organizationRequestDto == null ) {
            return null;
        }

        OrganizationBuilder<?, ?> organization = Organization.builder();

        organization.name( organizationRequestDto.getName() );
        organization.address( organizationRequestDto.getAddress() );

        return organization.build();
    }

    @Override
    public OrganizationResponseDto entityToResponseDto(Organization organization) {
        if ( organization == null ) {
            return null;
        }

        OrganizationResponseDto organizationResponseDto = new OrganizationResponseDto();

        return organizationResponseDto;
    }

    @Override
    public List<OrganizationResponseDto> listEntityToResponseDto(List<Organization> organizations) {
        if ( organizations == null ) {
            return null;
        }

        List<OrganizationResponseDto> list = new ArrayList<OrganizationResponseDto>( organizations.size() );
        for ( Organization organization : organizations ) {
            list.add( entityToResponseDto( organization ) );
        }

        return list;
    }
}
