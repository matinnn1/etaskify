package az.ingress.etaskify.repository.concrete;

import az.ingress.etaskify.dto.Request.TaskRequestDto;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.Query;

import java.util.Date;

public class TaskRepositoryImpl {
    @PersistenceContext
    private EntityManager entityManager;

    public void create(TaskRequestDto taskRequestDto){
        String sql = "INSERT INTO Tasks (assign_date, status, assigner_id, assigned_id, organization_id, task_id) VALUES VALUES(:assign_date, :status, :assigner_id, :assigned_id, :organization_id, :task_id)";

        Query query = entityManager.createNativeQuery(sql);
        query.setParameter("assign_date",new Date());
    }
}
