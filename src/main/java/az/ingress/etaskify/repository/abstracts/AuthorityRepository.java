package az.ingress.etaskify.repository.abstracts;


import az.ingress.etaskify.entity.Authority;
import az.ingress.etaskify.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {
    Authority findByRole(Role role);
}
