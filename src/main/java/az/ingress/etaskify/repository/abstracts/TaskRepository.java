package az.ingress.etaskify.repository.abstracts;

import az.ingress.etaskify.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<Task, Long> {
}
