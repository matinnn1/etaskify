package az.ingress.etaskify.repository.abstracts;

import az.ingress.etaskify.entity.Organization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@Repository
public interface OrganizationRepository extends JpaRepository<Organization, Long> {

    @Query(value = "select o from az.ingress.etaskify.entity.Organization o where o.user.email =:email")
    Optional<Organization> findByEmail(@Param("email") String email);
}