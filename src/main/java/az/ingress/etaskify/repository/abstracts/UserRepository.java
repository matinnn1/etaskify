package az.ingress.etaskify.repository.abstracts;

import az.ingress.etaskify.entity.User;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    @Override
    @EntityGraph(attributePaths = {"authorities"}, type = EntityGraph.EntityGraphType.FETCH)
    List<User> findAll();

    @Override
    @EntityGraph(attributePaths = {"authorities"}, type = EntityGraph.EntityGraphType.FETCH)
    Optional<User> findById(Long id);

    @EntityGraph(attributePaths = {"authorities"}, type = EntityGraph.EntityGraphType.FETCH)
    Optional<User> findByUsername(String username);
}
