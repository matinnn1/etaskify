package az.ingress.etaskify.repository.abstracts;

import az.ingress.etaskify.entity.Employee;
import az.ingress.etaskify.entity.Organization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    @Query(value = "select emp from az.ingress.etaskify.entity.Employee emp where emp.organization.id=:organizationId and emp.user.email =:email")
    Optional<Employee> findByOrganizationIdAndEmail(@Param("organizationId") Long organizationId, @Param("email") String email);

    @Query(value = "select emp from az.ingress.etaskify.entity.Employee emp where emp.organization.id=:organizationId and emp.user.Id =:userId")
    Optional<Employee> findByOrganizationIdAndUserId(@Param("organizationId") Long organizationId, @Param("userId") Long userId);

    @Query(value = "select emp from az.ingress.etaskify.entity.Employee emp where emp.organization.id=:organizationId")
    Optional<List<Employee>> findByOrganizationId(@Param("organizationId") Long organizationId);

    @Query(value = "select emp from az.ingress.etaskify.entity.Employee emp where emp.id IN :employeeIds")
    Optional<List<Employee>> findAllByIdIn(@Param("employeeIds") List<Long> employeeIds);
}
