package az.ingress.etaskify.config;

import az.ingress.etaskify.service.security.AuthFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AuthFilterConfigurerAdapter extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {
    private final AuthFilter authFilter;
    @Override
    public void configure(HttpSecurity http) {
        http.addFilterBefore(authFilter, UsernamePasswordAuthenticationFilter.class);
    }
}
