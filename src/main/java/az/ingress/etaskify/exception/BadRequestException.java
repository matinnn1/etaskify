package az.ingress.etaskify.exception;

public class BadRequestException extends GenericError {

    public BadRequestException(ErrorCodes code, Object... args) {
        super(code.code, code.toString(), 400, args);
    }

}
