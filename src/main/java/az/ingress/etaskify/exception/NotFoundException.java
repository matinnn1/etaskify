package az.ingress.etaskify.exception;

import org.springframework.http.HttpStatus;

public class NotFoundException extends GenericError {

    public NotFoundException(ErrorCodes errorCode, Object... arguments) {
        super(errorCode.code, errorCode.code, HttpStatus.NOT_FOUND.value(), arguments);
    }
}
