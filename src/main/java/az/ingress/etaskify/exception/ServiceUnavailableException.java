package az.ingress.etaskify.exception;

import org.springframework.http.HttpStatus;

import static az.ingress.etaskify.exception.ErrorCodes.INTERNAL_SERVER_ERROR;

public class ServiceUnavailableException extends GenericError {

    public ServiceUnavailableException() {
        super(INTERNAL_SERVER_ERROR.code, INTERNAL_SERVER_ERROR.code, HttpStatus.SERVICE_UNAVAILABLE.value());
    }
}
