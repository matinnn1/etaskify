package az.ingress.etaskify.exception;

import org.springframework.http.HttpStatus;

import static az.ingress.etaskify.exception.ErrorCodes.UNAUTHORIZED;

public class UnauthorizedException extends GenericError {

    public UnauthorizedException() {
        super(UNAUTHORIZED.code, UNAUTHORIZED.code, HttpStatus.UNAUTHORIZED.value());
    }
}
