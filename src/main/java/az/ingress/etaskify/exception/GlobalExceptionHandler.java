package az.ingress.etaskify.exception;

import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static az.ingress.etaskify.exception.ErrorCodes.BAD_REQUEST;
import static az.ingress.etaskify.exception.ErrorCodes.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpHeaders.ACCEPT_LANGUAGE;


@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler extends DefaultErrorAttributes {

    private static final String ARGUMENT_VALIDATION_FAILED = "Argument validation failed";

    @ExceptionHandler(RuntimeException.class)
    public final ResponseEntity<ErrorResponseDTO> handle(RuntimeException ex,
                                                         WebRequest request) {
        log.trace("Runtime Exception {}", ex.getMessage());
        ex.printStackTrace();
        return ofType(request, HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage(), INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(NotFoundException.class)
    public final ResponseEntity<ErrorResponseDTO> handle(NotFoundException ex,
                                                         WebRequest request) {
        log.trace("Resource not found {}", ex.getMessage());
        ex.printStackTrace();
        return ofType(request, HttpStatus.NOT_FOUND, ex.getMessage(), BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public final ResponseEntity<ErrorResponseDTO> handle(MethodArgumentTypeMismatchException ex,
                                                         WebRequest request) {
        log.trace("Method arguments are not valid {}", ex.getMessage());
        ex.printStackTrace();
        return ofType(request, HttpStatus.BAD_REQUEST, ex.getMessage(), BAD_REQUEST);
    }

    @ExceptionHandler(MismatchedInputException.class)
    public final ResponseEntity<ErrorResponseDTO> handle(MismatchedInputException ex,
                                                         WebRequest request) {
        log.trace("Mismatched input {}", ex.getMessage());
        ex.printStackTrace();
        return ofType(request, HttpStatus.BAD_REQUEST, ex.getOriginalMessage(), BAD_REQUEST);
    }

    @ExceptionHandler(ServiceUnavailableException.class)
    public final ResponseEntity<ErrorResponseDTO> handle(ServiceUnavailableException ex, WebRequest request) {
        log.trace("Service unavailable {}", ex.getMessage());
        ex.printStackTrace();
        return ofType(request, HttpStatus.SERVICE_UNAVAILABLE, ex.getMessage(), BAD_REQUEST);
    }

    @ExceptionHandler(BadRequestException.class)
    public final ResponseEntity<ErrorResponseDTO> handle(BadRequestException ex, WebRequest req) {
        log.trace("Bad request {}", ex.getMessage());
        ex.printStackTrace();
        var path = ((ServletWebRequest) req).getRequest().getRequestURL().toString();
        var requestedLanguage = getLanguage(req.getHeader(ACCEPT_LANGUAGE));
        return handleGenericError(ex, requestedLanguage, path);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public final ResponseEntity<ErrorResponseDTO> handle(MethodArgumentNotValidException ex, WebRequest request) {
        ex.printStackTrace();
        List<ConstraintsViolationError> validationErrors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(error -> new ConstraintsViolationError(error.getField(), error.getDefaultMessage()))
                .collect(Collectors.toList());
        return ofType(request, HttpStatus.BAD_REQUEST, ARGUMENT_VALIDATION_FAILED, validationErrors, BAD_REQUEST);
    }

    protected ResponseEntity<ErrorResponseDTO> ofType(WebRequest request, HttpStatus status, String message,
                                                      ErrorCodes errorCode) {
        return ofType(request, status, message, Collections.emptyList(), errorCode);
    }

    private ResponseEntity<ErrorResponseDTO> ofType(WebRequest request, HttpStatus status, String message,
                                                    List<ConstraintsViolationError> validationErrors,
                                                    ErrorCodes errorCode) {
        final ErrorResponseDTO response = ErrorResponseDTO
                .builder()
                .message(status.getReasonPhrase())
                .detail(message)
                .code(errorCode.code)
                .status(status.value())
                .path(((ServletWebRequest) request).getRequest().getRequestURI())
                .timestamp(OffsetDateTime.now())
                .requestedLang(getLanguage(request.getHeader(ACCEPT_LANGUAGE)))
                .build();
        validationErrors.forEach(
                validation -> response.getData().put(validation.getProperty(), validation.getMessage()));
        return ResponseEntity.status(status.value()).body(response);
    }

    public ResponseEntity<ErrorResponseDTO> handleGenericError(GenericError err, String lang, String path) {
        log.error("Error {}", err.toString());
        final ErrorResponseDTO response = ErrorResponseDTO
                .builder()
                .message(err.getCode())
                .detail(err.getMessage())
                .code(err.getCode())
                .status(err.getStatus())
                .path(path)
                .timestamp(OffsetDateTime.now())
                .requestedLang(lang)
                .build();
        return ResponseEntity.status(err.getStatus()).body(response);
    }

    private String getLanguage(String langHeader) {
        return ObjectUtils.isEmpty(langHeader) ? "az" : langHeader;
    }

}

