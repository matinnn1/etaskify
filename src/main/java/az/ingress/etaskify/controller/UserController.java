package az.ingress.etaskify.controller;

import az.ingress.etaskify.dto.Request.EmployeeRequestDto;
import az.ingress.etaskify.dto.Request.OrganizationRequestDto;
import az.ingress.etaskify.dto.Request.UserLoginDto;
import az.ingress.etaskify.dto.Request.UserRegisterDto;
import az.ingress.etaskify.dto.Response.EmployeeResponseDto;
import az.ingress.etaskify.dto.Response.OrganizationResponseDto;
import az.ingress.etaskify.dto.Response.TokenResponse;
import az.ingress.etaskify.entity.Employee;
import az.ingress.etaskify.entity.User;
import az.ingress.etaskify.service.abstracts.EmployeeService;
import az.ingress.etaskify.service.abstracts.OrganizationService;
import az.ingress.etaskify.service.concrete.UserManager;
import az.ingress.etaskify.service.security.JwtService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/api/user")
@Slf4j
public class UserController {
    private final UserManager userService;
    private final JwtService jwtService;
    private final OrganizationService organizationService;
    private final EmployeeService employeeService;

    @PostMapping("/register/organization")
    public ResponseEntity<TokenResponse> createOrganization(@RequestBody OrganizationRequestDto organizationRequestDto){
        log.info("Trying to create organization {}", organizationRequestDto);
        User user = organizationService.create(organizationRequestDto);
        return ResponseEntity.ok(jwtService.issueToken(user));
    }

    @PostMapping("/register/organization/{organizationId}/employee")
    public ResponseEntity<Employee> createEmployee(@PathVariable Long organizationId, @RequestBody EmployeeRequestDto employeeRequestDto){
        log.info("Trying to create employee {}", employeeRequestDto);
        var employeeResponseDto = employeeService.create(organizationId, employeeRequestDto);
        return ResponseEntity.ok(employeeResponseDto);
    }

    @PostMapping("/login")
    public ResponseEntity<TokenResponse> login(@RequestBody UserLoginDto loginDto){
        log.info("Trying to login user {}", loginDto);
        User user = userService.login(loginDto);
        TokenResponse tokenResponse = jwtService.issueToken(user);

        return ResponseEntity.ok(tokenResponse);
    }
}
