package az.ingress.etaskify.controller;

import az.ingress.etaskify.dto.Request.TaskRequestDto;
import az.ingress.etaskify.dto.Response.TaskResponseDto;
import az.ingress.etaskify.service.abstracts.TaskService;
import jakarta.persistence.EntityManager;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/api/task")
public class TaskController {
    private final TaskService taskService;

    @PostMapping
    public ResponseEntity<TaskResponseDto> create(@RequestBody TaskRequestDto taskRequestDto){

        TaskResponseDto taskResponseDto = taskService.create(taskRequestDto);

        return ResponseEntity.ok(taskResponseDto);
    }
}
