package az.ingress.etaskify.mapper;

import az.ingress.etaskify.dto.Request.EmployeeRequestDto;
import az.ingress.etaskify.dto.Request.OrganizationRequestDto;
import az.ingress.etaskify.dto.Response.EmployeeResponseDto;
import az.ingress.etaskify.dto.Response.OrganizationResponseDto;
import az.ingress.etaskify.entity.Employee;
import az.ingress.etaskify.entity.Organization;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface EmployeeMapper {
    Employee requestDtoToEntity(EmployeeRequestDto employeeRequestDto);
    EmployeeResponseDto entityToResponseDto(Employee Employee);
    List<EmployeeResponseDto> listEntityToResponseDto(List<Employee> Employees);
}
