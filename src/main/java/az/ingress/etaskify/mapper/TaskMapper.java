package az.ingress.etaskify.mapper;

import az.ingress.etaskify.dto.Request.TaskRequestDto;
import az.ingress.etaskify.entity.Task;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface TaskMapper {
    Task requestDtoToEntity(TaskRequestDto taskRequestDto);
}
