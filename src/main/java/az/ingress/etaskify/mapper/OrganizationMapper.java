package az.ingress.etaskify.mapper;

import az.ingress.etaskify.dto.Request.OrganizationRequestDto;
import az.ingress.etaskify.dto.Response.OrganizationResponseDto;
import az.ingress.etaskify.entity.Organization;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface OrganizationMapper {
    Organization requestDtoToEntity(OrganizationRequestDto organizationRequestDto);
    OrganizationResponseDto entityToResponseDto(Organization organization);
    List<OrganizationResponseDto> listEntityToResponseDto(List<Organization> organizations);
}
