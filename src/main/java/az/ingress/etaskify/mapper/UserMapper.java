package az.ingress.etaskify.mapper;

import az.ingress.etaskify.dto.Request.UserRegisterDto;
import az.ingress.etaskify.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface UserMapper {
    User requestDtoToEntity(UserRegisterDto userRequestDto);

}
