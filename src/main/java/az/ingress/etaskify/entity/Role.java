package az.ingress.etaskify.entity;

public enum Role {
    ADMIN,
    USER,
    CASHIER
}
