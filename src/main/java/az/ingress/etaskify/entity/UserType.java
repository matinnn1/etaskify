package az.ingress.etaskify.entity;

public enum UserType {
    EMPLOYEE,
    ORGANIZATION
}
