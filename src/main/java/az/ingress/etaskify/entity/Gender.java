package az.ingress.etaskify.entity;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Gender {
    MALE,
    WOMALE
}
