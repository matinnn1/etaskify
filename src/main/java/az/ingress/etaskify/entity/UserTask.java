package az.ingress.etaskify.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "users_tasks")
public class UserTask {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "assigner_employee_id")
    @ToString.Exclude
    private Employee assigner;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "assigned_employee_id")
    @ToString.Exclude
    private Employee assigned;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "organization_id")
    @ToString.Exclude
    private Organization organization;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "task_id")
    private Task task;

    @Column(nullable = false, updatable = false)
    @CreationTimestamp
    @DateTimeFormat(pattern = "dd-MM-yyyy hh:mm:ss")
    private LocalDateTime assignDate;



    @Enumerated(EnumType.STRING)
    private TaskStatus status;
}
