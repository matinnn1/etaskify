package az.ingress.etaskify.entity;

public enum TaskStatus {
    ASSIGNED,
    OPEN,
    IN_PROGRESS,
    READY_FOR_TESTING,
    TESTING,
    DONE,
    READY_FOR_DEPLOY
}
