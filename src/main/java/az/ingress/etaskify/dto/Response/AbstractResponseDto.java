package az.ingress.etaskify.dto.Response;

import jakarta.persistence.Column;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalDateTime;

public class AbstractResponseDto implements Serializable {
    private LocalDateTime createDate;
    private LocalDateTime updateDate;
}
