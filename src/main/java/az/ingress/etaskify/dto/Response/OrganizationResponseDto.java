package az.ingress.etaskify.dto.Response;

import az.ingress.etaskify.entity.Gender;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;

public class OrganizationResponseDto extends AbstractResponseDto{
    private Long id;
    private String name;
    private String username;
    private String email;
}
