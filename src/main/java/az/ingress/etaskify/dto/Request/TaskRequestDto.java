package az.ingress.etaskify.dto.Request;

import az.ingress.etaskify.entity.Priority;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.TreeSet;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TaskRequestDto {
    private String name;
    private String description;
    private Long assignerId;
    private List<Long> assignedIds;
    @Enumerated(EnumType.STRING)
    private Priority priority;
}
