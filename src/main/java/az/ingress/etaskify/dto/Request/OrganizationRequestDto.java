package az.ingress.etaskify.dto.Request;

import az.ingress.etaskify.entity.UserType;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class OrganizationRequestDto extends UserRegisterDto{
    private String name;
    private String address;
}
