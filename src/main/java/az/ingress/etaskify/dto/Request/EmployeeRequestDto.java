package az.ingress.etaskify.dto.Request;

import az.ingress.etaskify.entity.Gender;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeRequestDto extends UserRegisterDto{
    private String surname;
    @Enumerated(EnumType.STRING)
    private Gender gender;
}
