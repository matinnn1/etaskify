package az.ingress.etaskify.service.concrete;

import az.ingress.etaskify.dto.Request.UserLoginDto;
import az.ingress.etaskify.dto.Request.UserRegisterDto;
import az.ingress.etaskify.entity.*;
import az.ingress.etaskify.exception.BadRequestException;
import az.ingress.etaskify.mapper.UserMapper;
import az.ingress.etaskify.repository.abstracts.AuthorityRepository;
import az.ingress.etaskify.repository.abstracts.UserRepository;
import az.ingress.etaskify.service.abstracts.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static az.ingress.etaskify.exception.ErrorCodes.*;

@Service
@RequiredArgsConstructor
public class UserManager implements UserService {
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final AuthorityRepository authorityRepository;
    private final BCryptPasswordEncoder passwordEncoder;

    @Override
    public User register(UserRegisterDto userRegisterDto) {
        Optional<User> existUserOpt = userRepository.findByUsername(userRegisterDto.getUsername());

        if(existUserOpt.isPresent())
            throw new BadRequestException(USERNAME_ALREADY_REGISTERED);

        Role role = userRegisterDto.getUserType() == UserType.ORGANIZATION ? Role.ADMIN : Role.USER;

        Authority authority = authorityRepository.findByRole(role);

        User user = userMapper.requestDtoToEntity(userRegisterDto);
        user.setPassword(passwordEncoder.encode(userRegisterDto.getPassword()));
        user.setAccountNonExpired(true);
        user.setAccountNonLocked(true);
        user.setUpdateDate(null);
        user.setCredentialsNonExpired(true);

        user.setAuthorities(List.of(authority));

        return userRepository.save(user);
    }

    @Override
    public User login(UserLoginDto loginDto) {
        User user = userRepository.findByUsername(loginDto.getUsername())
                .orElseThrow(() -> new BadRequestException(USERNAME_NOT_FOUND));
        boolean passIsMatch = passwordEncoder.matches(loginDto.getPassword(), user.getPassword());//"$2a$10$kN6DLajS0DMK3M9m6i1WGen6QvhaUxB.L7WrqQISEjqhTc7HllOtC");

        if (passIsMatch)
            return user;

        else
            throw new BadRequestException(INVALID_CREDENTIALS);
    }

}
