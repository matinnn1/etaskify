package az.ingress.etaskify.service.concrete;

import az.ingress.etaskify.dto.Request.OrganizationRequestDto;
import az.ingress.etaskify.dto.Request.UserRegisterDto;
import az.ingress.etaskify.entity.User;
import az.ingress.etaskify.entity.Organization;
import az.ingress.etaskify.entity.UserType;
import az.ingress.etaskify.exception.BadRequestException;
import az.ingress.etaskify.mapper.OrganizationMapper;
import az.ingress.etaskify.repository.abstracts.OrganizationRepository;
import az.ingress.etaskify.service.abstracts.OrganizationService;
import az.ingress.etaskify.service.abstracts.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static az.ingress.etaskify.exception.ErrorCodes.*;

@Service
@RequiredArgsConstructor
public class OrganizationManager implements OrganizationService {
    private final OrganizationRepository organizationRepository;
    private final OrganizationMapper organizationMapper;
    private final UserService userService;
    @Override
    public User create(OrganizationRequestDto organizationRequestDto) {
        Optional<Organization> existOrganization = organizationRepository.findByEmail(organizationRequestDto.getEmail());

        if(existOrganization.isPresent())
            throw new BadRequestException(ORGANIZATION_ALREADY_EXIST);

        UserRegisterDto userRegisterDto = UserRegisterDto.builder()
                .username(organizationRequestDto.getUsername())
                .email(organizationRequestDto.getEmail())
                .password(organizationRequestDto.getPassword())
                .userType(UserType.ORGANIZATION)
                .build();

        User user = userService.register(userRegisterDto);

        Organization organization = organizationMapper.requestDtoToEntity(organizationRequestDto);
        organization.setUser(user);
        organizationRepository.save(organization);

        return user;
    }
}
