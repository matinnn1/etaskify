package az.ingress.etaskify.service.concrete;

import az.ingress.etaskify.dto.Request.TaskRequestDto;
import az.ingress.etaskify.dto.Response.TaskResponseDto;
import az.ingress.etaskify.entity.*;
import az.ingress.etaskify.exception.BadRequestException;
import az.ingress.etaskify.mapper.TaskMapper;
import az.ingress.etaskify.repository.abstracts.EmployeeRepository;
import az.ingress.etaskify.repository.abstracts.OrganizationRepository;
import az.ingress.etaskify.repository.abstracts.TaskRepository;
import az.ingress.etaskify.repository.abstracts.UserRepository;
import az.ingress.etaskify.service.abstracts.TaskService;
import az.ingress.etaskify.service.security.JwtService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import javax.swing.text.html.parser.Entity;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static az.ingress.etaskify.exception.ErrorCodes.*;

@Service
@RequiredArgsConstructor
public class TaskManager implements TaskService {
    private final TaskRepository taskRepository;
    private final UserRepository userRepository;
    private final EmployeeRepository employeeRepository;
    private final OrganizationRepository organizationRepository;
    private final JwtService jwtService;

    private final TaskMapper taskMapper;

    @Override
    @Transactional
    public TaskResponseDto create(TaskRequestDto taskRequestDto) {
        Employee assigner = employeeRepository.findById(taskRequestDto.getAssignerId()).orElseThrow(() -> new BadRequestException(USERNAME_NOT_FOUND));
        Optional<List<Employee>> assignedEmployeesOpt = employeeRepository.findAllByIdIn(taskRequestDto.getAssignedIds());

        Task task = taskMapper.requestDtoToEntity(taskRequestDto);
        List<UserTask> userTasks = new ArrayList<>();

        if (!assignedEmployeesOpt.isPresent())
            throw new BadRequestException(ASSIGNED_USERS_NOT_FOUND);

        for (Employee assignedEmployee : assignedEmployeesOpt.get()) {
            UserTask userTask = UserTask.builder()
                    .status(TaskStatus.ASSIGNED)
                    .assigner(assigner)
                    .assigned(assignedEmployee)
                    .organization(assigner.getOrganization())
                    .task(task)
                    .build();

            userTasks.add(userTask);
        }

        task.setUserTasks(userTasks);
        taskRepository.save(task);

        return new TaskResponseDto();
    }
}
