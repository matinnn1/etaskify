package az.ingress.etaskify.service.concrete;

import az.ingress.etaskify.dto.Request.EmployeeRequestDto;
import az.ingress.etaskify.dto.Request.UserRegisterDto;
import az.ingress.etaskify.dto.Response.EmployeeResponseDto;
import az.ingress.etaskify.entity.Employee;
import az.ingress.etaskify.entity.Organization;
import az.ingress.etaskify.entity.User;
import az.ingress.etaskify.entity.UserType;
import az.ingress.etaskify.exception.BadRequestException;
import az.ingress.etaskify.mapper.EmployeeMapper;
import az.ingress.etaskify.repository.abstracts.EmployeeRepository;
import az.ingress.etaskify.repository.abstracts.OrganizationRepository;
import az.ingress.etaskify.service.abstracts.EmployeeService;
import az.ingress.etaskify.service.abstracts.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static az.ingress.etaskify.exception.ErrorCodes.EMPLOYEE_ALREADY_EXIST;
import static az.ingress.etaskify.exception.ErrorCodes.ORGANIZATION_NOT_FOUND;

@Service
@RequiredArgsConstructor
public class EmployeeManager implements EmployeeService {
    private final EmployeeRepository employeeRepository;
    private final UserService userService;
    private final EmployeeMapper employeeMapper;
    private final OrganizationRepository organizationRepository;

    @Override
    public Employee create(Long organizationId, EmployeeRequestDto employeeRequestDto) {
        Organization organization = organizationRepository.findById(organizationId).orElseThrow(()->new BadRequestException(ORGANIZATION_NOT_FOUND));

        Optional<Employee> existEmployeeOpt = employeeRepository.findByOrganizationIdAndEmail(organizationId, employeeRequestDto.getEmail());

        if(existEmployeeOpt.isPresent())
            throw new BadRequestException(EMPLOYEE_ALREADY_EXIST);

        UserRegisterDto userRegisterDto = UserRegisterDto.builder()
                .username(employeeRequestDto.getUsername())
                .email(employeeRequestDto.getEmail())
                .password(employeeRequestDto.getPassword())
                .userType(UserType.EMPLOYEE)
                .build();

        User user = userService.register(userRegisterDto);

        Employee employee = employeeMapper.requestDtoToEntity(employeeRequestDto);
        employee.setUser(user);
        employee.setOrganization(organization);

        employee = employeeRepository.save(employee);

        return employee;
      //  return employeeMapper.entityToResponseDto(employee);
    }
}
