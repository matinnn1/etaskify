package az.ingress.etaskify.service.abstracts;

import az.ingress.etaskify.dto.Request.EmployeeRequestDto;
import az.ingress.etaskify.dto.Response.EmployeeResponseDto;
import az.ingress.etaskify.entity.Employee;
import az.ingress.etaskify.entity.User;

public interface EmployeeService {
    Employee create(Long organizationId, EmployeeRequestDto employeeRequestDto);
}
