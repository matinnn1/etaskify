package az.ingress.etaskify.service.abstracts;

import az.ingress.etaskify.dto.Request.UserLoginDto;
import az.ingress.etaskify.dto.Request.UserRegisterDto;
import az.ingress.etaskify.entity.User;

import java.util.Optional;

public interface UserService {
    User register(UserRegisterDto userRegisterDto);
    User login(UserLoginDto loginDto);
}
