package az.ingress.etaskify.service.abstracts;

import az.ingress.etaskify.dto.Request.TaskRequestDto;
import az.ingress.etaskify.dto.Response.TaskResponseDto;
import az.ingress.etaskify.entity.Task;

public interface TaskService {
    TaskResponseDto create(TaskRequestDto taskRequestDto);
}
