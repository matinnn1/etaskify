package az.ingress.etaskify.service.abstracts;

import az.ingress.etaskify.dto.Request.OrganizationRequestDto;
import az.ingress.etaskify.dto.Response.OrganizationResponseDto;
import az.ingress.etaskify.entity.User;

public interface OrganizationService {
    User create(OrganizationRequestDto organizationRequestDto);
}
