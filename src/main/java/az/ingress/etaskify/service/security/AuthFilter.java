package az.ingress.etaskify.service.security;

import io.jsonwebtoken.ExpiredJwtException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class AuthFilter extends OncePerRequestFilter {
    private final JwtService jwtService;
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        Optional<Authentication> authOptional = Optional.empty();

        try {
            authOptional = authOptional.or(()-> {
                return jwtService.getAuthentication(request);
            });
            authOptional.ifPresent(auth-> SecurityContextHolder.getContext().setAuthentication(auth));
            filterChain.doFilter(request,response);
        }
        catch(ExpiredJwtException exp) {
            throw new ServletException(exp.getMessage());
        }
    }
}
