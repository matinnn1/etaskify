package az.ingress.etaskify.service.security;

import az.ingress.etaskify.dto.Response.TokenResponse;
import az.ingress.etaskify.entity.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import jakarta.annotation.PostConstruct;
import jakarta.persistence.Convert;
import jakarta.persistence.Converter;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
@Service
public class JwtService {
    private Key key;
    @Value("${security.jwtProperties.secret}")
    private String secretKey;

    @PostConstruct
    public void init() {
        byte[] keyBytes = Decoders.BASE64.decode(secretKey);
        key = Keys.hmacShaKeyFor(keyBytes);
    }

    public TokenResponse issueToken(User user) {
        String accessToken = Jwts.builder()
                .id(user.getId().toString())
                .subject(user.getUsername())
                .issuedAt(new Date())
                .expiration(Date.from(Instant.now().plus(Duration.ofSeconds(120))))
                .claim("roles", user.getAuthorities().stream().map((aut) -> aut.getRole()).toList())
                .signWith(key)
                .compact();

        return new TokenResponse(accessToken);
    }

    public Long getUserId(HttpServletRequest request){
        Claims claims =  getClaims(request);
        return Long.parseLong(claims.getId());
    }

    public Claims getClaims(HttpServletRequest request){
        String autherization = request.getHeader("Authorization");
        if (autherization == null)
            return null;

        String token = autherization.substring("Bearer".length()).trim();

        Jws<Claims> claimsJws = Jwts.parser()
                .setSigningKey(key)
                .build()
                .parseSignedClaims(token);

        return  claimsJws.getPayload();
    }

    public Optional<? extends Authentication> getAuthentication(HttpServletRequest request) {
        Claims body = getClaims(request);

        if(body == null)
            return Optional.empty();

        if (body.getExpiration().before(new Date()))
            throw new RuntimeException("Token expired");

        return Optional.of(getAuthenticationBearer(body));
    }

    private Authentication getAuthenticationBearer(Claims claims) {
        List<String> roles = claims.get("roles", List.class);

        var authorityList = roles
                .stream()
                .map(role -> new SimpleGrantedAuthority(role))
                .collect(Collectors.toList());

        var details = new org.springframework.security.core.userdetails.User(
                claims.getSubject(),
                "",
                authorityList
        );


        return new UsernamePasswordAuthenticationToken("",details,authorityList);
    }
}
